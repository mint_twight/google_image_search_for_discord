require 'dotenv'
Dotenv.load

WORKING_DIR = File.expand_path(File.dirname(__FILE__))

God.port = ENV.fetch('GOD_PORT', 17166).to_i
God.watch do |w|
  w.name     = 'gis_discord'
  w.dir      = WORKING_DIR
  w.start    = "ruby #{WORKING_DIR}/app.rb"
  w.restart  = "kill `cat gis_pid`"
  w.interval = 5.seconds

  w.start_if do |start|
    start.condition(:process_running) do |c|
      c.running = false
    end
  end

  w.restart_if do |restart|
    restart.condition(:cpu_usage) do |c|
      c.interval = 10.seconds
      c.above    = 25.percent
      c.times    = 5
    end
  end
end
