require 'discordrb'
require "./utils/discord_util"
include DiscordUtil

`echo #{Process.pid} > gis_pid`

bot = Discordrb::Commands::CommandBot.new(
  token:     ENV["DISCORD_TOKEN"],
  client_id: ENV["DISCORD_CLIENT_ID"],
  prefix:    '/')

bot.command :ping do |event|
  m = event.respond("Pong！")
  m.edit "Pong！ 応答までに #{Time.now - event.timestamp} 秒かかったよ！"
end

bot.message(start_with: '/gis') do |event|
  begin
    parsed_text = option_parser(event.message.content.gsub('　', ' '))
    (event.respond(gis_usage) and next) if parsed_text[:help_message] || parsed_text[:keyword].empty?

    parsed_text[:position].each do |position|
      params = parsed_text.merge(position: position)

      if parsed_text[:multi]
        parsed_text[:keyword].each { |keyword| send_message(event, [keyword], params) }
      else
        send_message(event, params[:keyword], params)
      end
    end
  rescue => e
    event.respond("#{e}")
    puts e
    puts e.backtrace
  end
end

bot.command :live_help do |event|
  event.respond(live_usage)
end

bot.command :mugi_live do |event|
  event.respond('https://www.youtube.com/channel/UC_GCs6GARLxEHxy1w40d6VQ/live')
end

bot.command :mint_live do |event|
  event.respond('https://www.youtube.com/channel/UCr7Fsw1ykdDC65e00plOKdg/live')
end

bot.run
