class Picture < ActiveRecord::Base
  validates  :keyword, :position, :short_url, :raw_url, presence: true

  def get_url(raw = false)
    count_up!
    return short_url if short_url && !raw
    raw_url
  end

  def count_up
    self.count += 1
  end

  def count_up!
    count_up
    save
  end
end
