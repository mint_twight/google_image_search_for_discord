require 'dotenv'
Dotenv.load
require 'active_support/core_ext/date'
require "./active_record_config"
require "./discord_client"
