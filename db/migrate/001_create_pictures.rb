# スキーマの設定
class CreatePictures < ActiveRecord::Migration[4.2]
  def self.up
    create_table :pictures do |t|
      t.string  :keyword
      t.string  :short_url
      t.string  :raw_url
      t.integer :position, default: 0
      t.integer :count,    default: 1

      t.timestamp
    end
  end

  def self.down
    drop_table :pictures
  end
end

# マイグレーション
# CreatePictureTable.migrate(:up)
