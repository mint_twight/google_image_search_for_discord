require "active_record"
# データベースへの接続
ActiveRecord::Base.establish_connection(
  adapter:   'sqlite3',
  database:  ENV.fetch('DISCORD_CLIENT_DB_FILE', 'gis_for_discord.sqlite3')
)

# migrationファイルの読み込み
Dir[File.expand_path('../migrate', __FILE__) << '/*.rb'].each do |migration|
  require migration
end

# modelファイルの読み込み
Dir[File.expand_path('../models', __FILE__) << '/*.rb'].each do |model|
  require model
end
