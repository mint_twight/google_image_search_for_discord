module DiscordUtil
  require 'optparse'
  require 'bitly'
  require_relative '../google_image_search_js'

  Bitly.use_api_version_3
  Bitly.configure do |config|
    config.api_version = 3
    config.login       = ENV["BITLY_LOGIN"]
    config.api_key     = ENV["BITLY_API_KEY"]
  end

  def option_parser(text)
    text = text.split[1..-1]
    opts = {:position => [0]}
    opt = OptionParser.new do |opt|
      opt.on('-p', '--position Number', Array, 'Change scraping position.') do |num|
        opts[:position] = num.map(&:to_i)
      end
      opt.on('-e', '--except Words', Array, 'Search excpting input words.') do |words|
        opts[:except] = words.map{ |word| word.insert(0, '-') }
      end
      opt.on('-m', '--multi', 'Multi search.') do |bool|
        opts[:multi] = bool
      end
      opt.on('-r', '--raw', 'Do not use short URL.') do |bool|
        opts[:raw] = bool
      end
      opt.on('-u', '--update', 'Update Database.') do |bool|
        opts[:update] = bool
      end
      opt.on_tail("-h", "--help", "Show this message") do |bool|
        opts[:help_message] = bool
      end
    end
    opts[:keyword] = opt.parse!(text)
    return opts
  end

  def gis_usage
    <<~'EOS'
      Usage: /gis keyword [options]
        -p, --position Number            Change scraping position.
        -e, --except   Words             Search excpting input words.
        -m, --multi                      Multi search.
        -r, --raw                        Do not use short URL.
        -u, --update                     Update Database.
        -h, --help                       Show this message.
    EOS
  end

  def live_usage
    <<~'EOS'
      /ping      #疎通確認用
      /mugi_live #むぎむぎのLIVE配信LINK
      /mint_live #初鹿みんとのLIVE配信LINK
    EOS
  end

  def send_message(event, keyword, params)
    k             = (keyword.to_a + params[:except].to_a).join(' ')
    m             = event.respond("[#{k}]のURL取得中")
    pic           = Picture.find_or_initialize_by(keyword: k, position: params[:position])
    return m.edit("検索ワード: #{k}\n取得位置: #{pic.position}\nURL: #{pic.get_url(params[:raw])}") if pic.persisted? && !params[:update]

    raw_url, position = GoogleImageSearchJs.new(keyword, params).search

    if raw_url.nil?
      m.edit("検索ワード: #{k}\ndid not match any image results.")
    else
      short_url = Bitly.client.shorten(raw_url).short_url
      pic.update_attributes({short_url: short_url, raw_url: raw_url})
      m.edit("検索ワード: #{k}\n取得位置: #{position}\nURL: #{params[:raw] ? raw_url : short_url}")
    end
  end
end
